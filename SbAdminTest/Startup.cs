﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SbAdminTest.Startup))]
namespace SbAdminTest
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
